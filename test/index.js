/**
 * Basic tests for the plugin
 */
const { PassThrough } = require('stream');
const { expect } = require('chai');
const File = require('vinyl');
const PluginError = require('plugin-error');
const gulpModifier = require('../');

describe('gulp-modifier', function () {

    it('should rock', function () { return true; });

    it('should pass null files along un-modified', function (done) {

        const fakeFile = new File({
            contents: null
        });

        const modifier = gulpModifier();

        modifier.once('data', (file) => {

            expect(file.isNull()).to.be.true;
            expect(file.contents).to.be.null;
            done();
        });

        modifier.write(fakeFile);
    });

    it('should error when given a stream', function () {

        const fakeStream = new PassThrough();
        fakeStream.write('some');
        fakeStream.write('data');
        fakeStream.end();

        const fakeFile = new File({
            contents: fakeStream
        });

        const modifier = gulpModifier();

        const shouldThrow = () => { modifier.write(fakeFile); };

        expect(shouldThrow).to.throw(PluginError);
    });

    it('should return a Buffer if given a Buffer', function (done) {

        const fakeFile = new File({
            contents: Buffer.from('a buffer with this content')
        });

        const modifier = gulpModifier();

        modifier.once('data', (file) => {

            expect(file.isBuffer()).to.be.true;
            done();
        });

        modifier.write(fakeFile);
    });

    it('should do nothing if not given a modifier function', function (done) {

        const fileContents = 'a buffer with this content';

        const fakeFile = new File({
            contents: Buffer.from(fileContents)
        });

        const modifier = gulpModifier(false);

        modifier.once('data', (file) => {

            expect(file.contents.toString()).to.equal(fileContents);
            done();
        });

        modifier.write(fakeFile);
    });

    it('should apply the modifier function to the file contents', function (done) {

        const fakeFile = new File({
            contents: Buffer.from('a buffer with content')
        });

        const modifier = gulpModifier(content => content.replace('content', 'awesomeness'));

        modifier.once('data', (file) => {

            expect(file.contents.toString()).to.equal('a buffer with awesomeness');
            done();
        });

        modifier.write(fakeFile);
    });
});
