gulp-modifier
=============
Modify the contents of files in a gulp stream with an arbitrary modifier function.

```js
const gulp = require('gulp');
const gulpModifier = require('gulp-modifier');

function modify() {

    return gulp.src('src/files/**')
        .pipe(gulpModifier((contents, path) => {

            let newContents = contents.replace('lame', 'awesome');

            newContents += path;

            return newContents.trim();
        }))
        .pipe(gulp.dest('./dist/files/'));
}

exports.modify = modify;
```

This is based on [gulp-modify](https://github.com/efacilitation/gulp-modify) and
[gulp-modify-file](https://github.com/ifedu/gulp-modify-file) both of which seem to be no longer
maintained or not up to speed with gulp 4 methodology.

The modifier function you pass in will get the following arguments:

- `contents` a `String` with the full contents of the file
- `path` a `String` with the path to the file
- `file` a vinyl `File` object representing the file being modified

Just return the new contents of the file from your function.
