/**
 * Modify the contents of a file with a user provided function.
 *
 * Based on: https://github.com/efacilitation/gulp-modify
 */
const through = require('through2');
const PluginError = require('plugin-error');

// Define the plugin name
const PLUGIN_NAME = 'gulp-modifier';

module.exports = modifierFn => (
    through.obj((file, encoding, callback) => {

        // Nothing to do
        if (file.isNull()) {

            callback(null, file);
            return;
        }

        // file.contents is a Stream - https://nodejs.org/api/stream.html
        if (file.isStream()) {


            callback(new PluginError(PLUGIN_NAME, 'Streams not supported!'));
            return;
        }

        // Sanity check
        if (! file.isBuffer()) {

            callback(new PluginError(PLUGIN_NAME, 'Unexpected file type!'));
            return;
        }

        // If we weren't passed a function, just pass along the file unmodified
        if (! modifierFn || typeof modifierFn !== 'function') {

            callback(null, file);
            return;
        }

        // Run the provided function and pass on the results
        const modifiedContents = modifierFn(file.contents.toString(), file.path, file);

        file.contents = Buffer.from(modifiedContents);

        callback(null, file);
    })
);
